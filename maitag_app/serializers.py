from rest_framework import serializers 

from .models import (
    Utterance, 
    Intent, 
    LabelInstance, 
    MLModel, 
    Classifier,
    Project
)
from users.models import CustomUser



class MLModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = MLModel
        fields = [
            'id',
            'name',
            'path',
        ]


class ClassifierSerializer(serializers.ModelSerializer):

    class Meta:
        model = Classifier
        fields = [
            'id',
            'name',
            'mlmodel',
        ]


class IntentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Intent 
        fields = [
            'id',
            'name',
        ]


class LabelInstanceSerializer(serializers.ModelSerializer):

    class Meta:
        model = LabelInstance
        fields = [
            'id',
            'intent',
            'description',
            'classifier',
        ]



class UtteranceSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Utterance 
        fields = [
            'id',
            'user',
            'text',
            'intents',
        ]


class ProjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = Project 
        fields = [
            'id',
            'name',
            'utterances',
        ]


