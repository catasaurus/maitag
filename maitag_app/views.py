from django.http import JsonResponse
from rest_framework import generics

from .models import (
    Utterance, 
    Intent, 
    LabelInstance, 
    MLModel, 
    Classifier, 
    Project
)
from .serializers import (
    UtteranceSerializer, 
    IntentSerializer, 
    LabelInstanceSerializer,
    MLModelSerializer,
    ClassifierSerializer,
    ProjectSerializer
)



class MLModelList(generics.ListCreateAPIView):

    queryset = MLModel.objects.all()
    serializer_class = MLModelSerializer


class MLModelDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = MLModel.objects.all()
    serializer_class = MLModelSerializer  


class ClassifierList(generics.ListCreateAPIView):

    queryset = Classifier.objects.all()
    serializer_class = ClassifierSerializer


class ClassifierDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = Classifier.objects.all()
    serializer_class = ClassifierSerializer


class IntentList(generics.ListCreateAPIView):

    queryset = Intent.objects.all()
    serializer_class = IntentSerializer


class IntentDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = Intent.objects.all()
    serializer_class = IntentSerializer


class LabelInstanceList(generics.ListCreateAPIView):

    queryset = LabelInstance.objects.all()
    serializer_class = LabelInstanceSerializer


class LabelInstanceDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = LabelInstance.objects.all()
    serializer_class = LabelInstanceSerializer


class UtteranceList(generics.ListCreateAPIView):

    queryset = Utterance.objects.all()
    serializer_class = UtteranceSerializer


class UtteranceDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = Utterance.objects.all()
    serializer_class = UtteranceSerializer


class ProjectList(generics.ListCreateAPIView):

    queryset = Project.objects.all()
    serializer_class = ProjectSerializer


class ProjectDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = Project.objects.all()
    serializer_class = ProjectSerializer



# Create a new table called Prediction in models.py
# Put request.JSON and response.JSON dictionary keys and values into this table
# ... loading


# Create a function based view that always replies with response.json (render.response) from the one that Maria gives us

def predict(request):

    data = {
        "clusters": 
        [
            {
              "cluster_name": "I love you Maya",
              "suggested_intent": "i_love_you",
              "cluster_size": 1,
              "utterances": 
              [
                  {
                   "id": "<string or number >",
                   "text": "I love you Maya",
                  },
              ]
            },  
            {
              "cluster_name": "What is this",
              "suggested_intent": None,
              "cluster_size": 1,
              "utterances":
              [
                  {
                   "id": "<string or number >",
                   "text": "What is this",
                  },
              ]
            },
        ]
    }

    return JsonResponse(data)








""" Concrete View Classes
# CreateAPIView
Used for create-only endpoints.
# ListAPIView
Used for read-only endpoints to represent a collection of model instances.
# RetrieveAPIView
Used for read-only endpoints to represent a single model instance.
# DestroyAPIView
Used for delete-only endpoints for a single model instance.
# UpdateAPIView
Used for update-only endpoints for a single model instance.
# ListCreateAPIView
Used for read-write endpoints to represent a collection of model instances.
RetrieveUpdateAPIView
Used for read or update endpoints to represent a single model instance.
# RetrieveDestroyAPIView
Used for read or delete endpoints to represent a single model instance.
# RetrieveUpdateDestroyAPIView
Used for read-write-delete endpoints to represent a single model instance.
"""