from django.contrib import admin
from .models import MLModel, Classifier, Intent, Utterance



admin.site.register(MLModel)
admin.site.register(Classifier)
admin.site.register(Intent)
admin.site.register(Utterance)
