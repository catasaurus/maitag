from django.urls import path  

from .views import (
    UtteranceList, 
    UtteranceDetail,
    IntentList,
    IntentDetail,
    LabelInstanceList,
    LabelInstanceDetail,
    MLModelList,
    MLModelDetail,
    ClassifierList,
    ClassifierDetail,
    ProjectList,
    ProjectDetail,
    predict
)



app_name = 'maitag_app'

urlpatterns = [
    path('utterances/', UtteranceList.as_view(), name = 'utterance-list'),
    path('utterances/<int:pk>/', UtteranceDetail.as_view(), name = 'utterance-detail'),
    path('intents/', IntentList.as_view(), name = 'intent-list'),
    path('intents/<int:pk>/', IntentDetail.as_view(), name = 'intent-detail'),
    path('labels/', LabelInstanceList.as_view(), name = 'label-list'),
    path('labels/<int:pk>/', LabelInstanceDetail.as_view(), name = 'label-detail'),
    path('mlmodels/', MLModelList.as_view(), name = 'mlmodel-list'),
    path('mlmodels/<int:pk>/', MLModelDetail.as_view(), name = 'mlmodel-detail'),   
    path('classifiers/', ClassifierList.as_view(), name = 'classifier-list'),
    path('classifiers/<int:pk>/', ClassifierDetail.as_view(), name = 'classifier-detail'), 
    path('projects/', ProjectList.as_view(), name = 'project-list'),
    path('projects/<int:pk>/utterances/', ProjectDetail.as_view(), name = 'project-detail'),
    path('predict/', predict)
]