#  How to use the `CountVectorizer` in Sklearn

0. Set up our example document:

```python
sentences = [
    'This book is great',
    'This book was so bad'
]
```

1. Initialize the `CountVectorizer()` object:

```python
vectorizer = CountVectorizer()
```
2. (Fit) our document to preproit, split up into words or characters, and convert it into an array of numbers that can do math. Found all of the different words in the text and counted how many of each there were:

```python
X = vectorizer.fit_transform(sentences) 
```
Output:

```terminal
  (0, 5)    1 
  (0, 1)    1
  (0, 3)    1
  (0, 2)    1
  (1, 5)    1
  (1, 1)    1
  (1, 6)    1
  (1, 4)    1
  (1, 0)    1
```
Explaination:

(0, 5)

* **0**: 
First sentence (This book is great)

* **5**: 
The position of the first token of the first sentence in the vocabulary:
(['bad', 'book', 'great', 'is', 'so', **'this'**, 'was'])

* **1**:
How many of **this** token was in the sentence

3. Extract the vocabulary:

```python
vectorizer.get_feature_names_out()
```
Output:

```terminal
['this', 'book', 'is', 'great', 'was', 'so', 'bad']

['bad', 'book', 'great', 'is', 'so', 'this', 'was']
```

4. Convert the count given to words into an Numpy array:

```python
X.toarray()
```
Output:

```terminal
[[0 1 1 1 0 1 0]
 [1 1 0 0 1 1 1]]
```
Explaination:

```
                               bad    book   great   is   so   this   was

This book is great              0      1       1      1    0    1      0    

This book was so bad            1      1       0      0    1    1      1

```



