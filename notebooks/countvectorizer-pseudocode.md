# The `CountVectorizer` in Pseudocode format

## Pseudocode

Start program

* Read documents

* Read vocabs

* Read bow

* FOR sentences

    * Split up sentence into characters

    * Add characters to vocabs

* ENDFOR

* Remove duplicate items from vocabs

* Order vocabs alphabetically

* FOR sentences
    
    * Read vocabs

    * FOR vocabs

        * IF vocab exists in sentence

            * Change vocab value to how many times the vocab exists

        * ELSE

            * Change vocab value to 0

        * ENDIF

    * ENDFOR

    * Add vocabs to bow

* ENDFOR

* Print bow

End program

## Translation

```python
sentences = [
    'I read the best book ever',
    'I read the most weird book and I hate it'
]

vocabs = []

bow = []

for sentence in sentences:
   tokens = sentence.split()
   for token in tokens:
       vocabs.append(token)

for sentence in sentences:
    items = sorted(set(vocabs))
    for item in items:
        if item in sentence:
            items[items.index(item)] = sentence.count(item)
        else:
            items[items.index(item)] = 0
    bow.append(items)

print(bow)
```

