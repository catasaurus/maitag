from rest_framework import serializers

from .models import CustomUser, Profile



class CustomUserSerializer(serializers.ModelSerializer):
    """
    - checks user credentials are valid
    - translates and saves user data into JSOM format
    """

    class Meta:
        model = CustomUser
        fields = ['id', 'email', 'username', 'password']
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = CustomUser(
            email = validated_data['email'],
            username = validated_data['username']
        )
        user.set_password(validated_data['password'])
        user.save()
        return user


class ProfileSerializer(serializers.ModelSerializer):
    """translates the Profile instances into JSON format"""

    class Meta:
        model = Profile
        fields = '__all__'


class ResetPasswordRequestSerializer(serializers.Serializer):

    email = serializers.EmailField(max_length = 255)


class ResetPasswordSerializer(serializers.Serializer):

    password = serializers.CharField(max_length = 30)
