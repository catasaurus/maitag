from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.encoding import force_bytes, force_str
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from rest_framework import generics, status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView


from .models import CustomUser, Profile
from .serializers import (
    CustomUserSerializer, 
    ProfileSerializer, 
    ResetPasswordRequestSerializer, 
    ResetPasswordSerializer
)
from .tokens import reset_password_token



class CustomUserList(generics.ListAPIView):
    """Reads CustomUser model instances"""

    queryset = CustomUser.objects.all()
    serializer_class = CustomUserSerializer


class ProfileList(generics.ListAPIView):
    """Reads Profile model instances"""

    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer


class RegisterView(APIView):
    """
    - sends user credentials to the server
    - checks users credentials are valid
    - creates a new user
    """

    def get(self, request):
        return Response(reverse('users:register')) # due to the namespace

    def post(self, request):
        serializer = CustomUserSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status = status.HTTP_201_CREATED)
        return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)


class ResetPasswordRequest(APIView):

    def get(self, request):
        return Response(reverse('users:password-reset-request'))

    def post(self, request, format = 'json'):
        serializer = ResetPasswordRequestSerializer(data = request.data)
        """Verifies the credentials"""
        if serializer.is_valid():
            current_site = get_current_site(request).domain
            mail_subject = 'Reset your password'
            """Throws an error when user's email is not existed"""
            try:
                user = CustomUser.objects.get(email = serializer.data['email'])
            except(CustomUser.DoesNotExist):
                return Response('Email does not exist', status = status.HTTP_400_BAD_REQUEST)  
            """Send the mail to the user's email when it's existed"""  
            """Setting mail dependencies"""
            message = render_to_string('users/reset-password-request.html', {
                'user': user,
                'domain': current_site,
                'uid':urlsafe_base64_encode(force_bytes(user.pk)),
                'token':reset_password_token.make_token(user),
            })
            """Sends the mail"""
            send_mail(mail_subject, message, None, [user.email], fail_silently = False)
            return Response('Check your email, we sent a link to reset your password.')  
        return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)


@api_view(('GET', 'POST',))
def activate_reset(request, uidb64, token, format = 'json'):
    """Throws an error in case if the user is not existed"""
    try:
        uid = force_str(urlsafe_base64_decode(uidb64))
        user = CustomUser.objects.get(pk = uid)
    except(TypeError, ValueError, OverflowError, Contributor.DoesNotExist):
        user = None
    """Activates the reset password link so the user can reset the password"""
    if user is not None and reset_password_token.check_token(user, token):
        serializer = ResetPasswordSerializer(data = request.data)
        if serializer.is_valid():
            json = serializer.data
            user.set_password(json['password'])
            user.save()         
            return Response('Your password changed successfully, you can login now.')
        else:
            return Response(serializer.errors)
    return Response('Token is invalid! try again', status = status.HTTP_400_BAD_REQUEST)





""" Concrete View Classes
# CreateAPIView
Used for create-only endpoints.
# ListAPIView
Used for read-only endpoints to represent a collection of model instances.
# RetrieveAPIView
Used for read-only endpoints to represent a single model instance.
# DestroyAPIView
Used for delete-only endpoints for a single model instance.
# UpdateAPIView
Used for update-only endpoints for a single model instance.
# ListCreateAPIView
Used for read-write endpoints to represent a collection of model instances.
RetrieveUpdateAPIView
Used for read or update endpoints to represent a single model instance.
# RetrieveDestroyAPIView
Used for read or delete endpoints to represent a single model instance.
# RetrieveUpdateDestroyAPIView
Used for read-write-delete endpoints to represent a single model instance.
"""