## MAITag - Machine-Assisted Intent Tagging

1. Django Rest Framework + token-based oauth for models.py tables: Message, Tag, Tagger deployed to render.com using gitlab.com CI-CD pipeline (a template app that does all this will be available). 

2. A REST api that allows an external app to upload a table (csv, or json) containing short English utterances, (less than a sentence) some utterances will have categorical labels for their intent (such as "yes", "no", "maybe", "help"), other utterances will have a blank, null or "unknown" intent label.

3. Data model Utterance and Label and Classifier tables, all 3 with 'created' and 'updated' fields. Many2Many relationship between the Label-to-Utterance tables with a through table that specifies the source and timestamp of the label (ML supervised model ID, ML unsupervised model ID, API POST, human username/email, latest canonical label based on the weighted average of existing labels)

4. administrator can click a button to train a supervised ML text classification model to classify some of the "unknown" utterances with the existing class labels in the dataset
    - 4a. rest api or GUI download of the revised (relabeled) table of utterances, some of which have been labeled, but many "unknowns" remain.
    - 4b. Django admin interface view to allow an administrator to correct or confirm any machine learning labels of the "unknown" utterances.
    - 4c. Django rest api that allows external app to change or confirm the class label for any utterance in the database administrator can click a button to fits an unsupervised model to ALL utterances in the database with an initial K (num clusters) of num_existing_intent_categories + ceil(num_unknown_utterances / 5)
    
5. unsupervised clustering model is evaluated by IOU (intersection over union) of clusters with known labels, unknown labels evaluated with silhouette score. the average of these two % scores is the model tuning metric that is optimized based on "grid search" (looping over other possible K values between 1 and 10.
7. unknown K clusters are each labeled in the database with the shortest utterance found in each cluster (labeled by the best unsupervised model), the previous "unknown" labels remain in the database along with the new machine-estimated labels
8. any new utterance(s) uploaded to the system through the API triggers an intent classification response that can label the new utterance with an existing intent label or a new one automatically
9. administrator can create a new chatbot ID within a new Chatbot table in models.py with an one to many FK to the utterances table. (chatbot FK field in the utterance table)

## Svelte chatbot widget job description (for UpWork)

Build a prototype Svelte chatbot widget

Goal is to port the popout chatbot widget called Poly to Svelte. 
The typescript implementation is here: https://wespeaknyc.cityofnewyork.us/  (parrot icon in lower right)

A similar Django app (with CSS and HTML to get you started) is here:
https://www.qary.ai/poly/

And a Svelte app that talks to a Django REST API is available here:
https://playground.proai.org/

We have front-end and back-end source code for all of these apps that we will share with you.

And of course you can get started at https://svelte.dev/repl