# Sprint 7 (Sep 30 - Oct 07)

* [x] R0.2-0.2: Overwrite/edit the original Hobson's MAYA notebook 
* [x] R2-1: Update the initial database diagram 
* [x] R0.5-0.2: Alter/Update the database tables based on the database diagram
* [x] R0.1-0.1: Add project dependencies to **requirements.txt**
* [x] R0.1-0.1: Create a build script for render deployement
* [x] R0.2-0.2: Create another script to create a superuser on production
* [x] R0.5-0.2: Create a web service on Render to deploy `MAITAG`
* [x] R0.2-0.1: Add environement variables needed for the web service
* [x] R0.2-0.1: Push the changes and deploy 
* [x] R1-0.8: Setup the configuration needed to serve the static files in production
* [x] R2-1.2: Fix the CSS issues with the Django admin site. 
* [x] R1-0.2: Test the app and the authentication system in production

## Done: Sprint 6 (Sep 23 - Sep 30)

* [x] R1-1: Read/understand what word embedding is and why we need it
* [x] R1-1: Run/understand the cells that do encoding, splitting, training, and some testing
* [x] R0.5-0.2: Filter out the suspicious/unknown rows
* [x] R0.2-0.2: Group the dataframe by intent and highlight the first utterance in every intent
* [x] R0.5-0.2: Update the intent column by adding some corrections
* [x] R1-0.5: Revise the unknown values
* [x] R1-0.5: Overwrite the intent column with the new revised column `your_corrected`
* [x] R0.2-0.2: Save the file as a CSV file 
* [x] R0.2-0.2: Train and test the model with the new corrected data
* [x] R0.1-0.1: Share with your team your Hobson's MAYA notebook copy