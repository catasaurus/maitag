# Sprint 1 (Jul 15 - Jul 22)

* [x] R1-0.1: Created an Initial Django project with users app
* [x] R1-0.5: Created a Custom User model based on BaseUserManager/AbstractBaseUser
* [x] R1-0.5: Created a Profile model that inherits from User table
* [x] R1-0.5: Added signals to create profiles automatically once the User is created
* [x] R1-0.2: Customized the admin interface for CRUDing accounts/profiles
* [x] R1-0.2: Configure JWT-based authentication settings using `dj-rest-auth`
* [x] R1-0.5: Transform the CustomUser and Profile models into a Restful API
* [x] R1-0.5: Create a custom API endpoint to register new users
* [x] R1-0.5: Implement the authentication (login/logout) endpoints
* [x] R1-1: Create and activate a custom API endpoint to reset password



